#!/usr/bin/env python 
# -*- coding: utf-8 -*-

import numpy as np 
import matplotlib.pyplot as plt
import midi
from matplotlib.collections import LineCollection
from optparse import OptionParser

def search_track_name(midi, name):
    """Search for a TrackName inside a midi object.
    Returns: Trackindex
    """

    for i, m in enumerate(midi):
        for key in m:
            if match(key, name):
                return i 
    else:
        print "Track %s not found" % name

def match(key, name):
    """Test if  a TrackNameEvent matches with a key"""
    if type(key) == midi.events.TrackNameEvent:
        expected = "".join([chr(c) for c in key.data])
        if name in expected:
            return True
        else:
            return False
    return False

def get_formatted_notes(midi_keys):
    """Get a numpy array with formatted output from a midikey-list

    Input: midi keylist or tuple
    Output: Array with starttime, duration and pitch
    """
    note_list = []
    note_buffer = []
    for note, time, velocity in midi_keys:
        if velocity!=0:
            note_buffer.append([time, note])
        if velocity == 0:
            for elem in note_buffer:
                if elem[1]==note:
                    note_list.append([elem[0], time-elem[0], note]) 
                    break
            note_buffer.remove(elem)
    return np.array(note_list)

def plot_score(left_hand, right_hand):
    """Plot the musical score for the left and right hand"""
    f, (ax1, ax2) = plt.subplots(2,sharex=True)

    rxmin = right_hand[:,0].min()
    rxmax = right_hand[:,0].max()

    rymin = right_hand[:,2].min()
    rymax = right_hand[:,2].max()

    lymin = left_hand[:,2].min()
    lymax = left_hand[:,2].max()

    ax1.set_xlim(rxmin, rxmax)
    ax1.set_ylim(rymin, rymax)
    ax2.set_ylim(lymin, lymax)
  
    ax1.set_ylabel("Right Hand")
    ax2.set_ylabel("Left Hand")
    ax2.set_xlabel("Time")

    l = create_line_collection(left_hand)
    r = create_line_collection(right_hand)
    ax1.add_collection(r)
    ax2.add_collection(l)
    plt.show()

def create_line_collection(data):
    """Create a LineCollection for faster plotting"""
    lines = []
    for x in data:
        lines.append([(x[0],x[2]),(x[0]+x[1],x[2])])
    line_segments = LineCollection(lines,color = "blue",
                                        linestyles='solid', lw =8)
    return line_segments

def get_midi_keys(miditrack):
    """Reade midi keys and attributes from midi object"""
    notes, ticks, velocity = [], [], []
    for key in miditrack:
        if type(key) == midi.events.NoteOnEvent:
            notes.append(key.pitch)
            ticks.append(key.tick)
            velocity.append(key.velocity)
    notes = np.array(notes)
    total_ticks = np.array(ticks).cumsum()
    velocity = np.array(velocity)
    return zip(notes, total_ticks, velocity)
    
def main():
    parser = OptionParser()
    parser.add_option("-f", "--file",action="store", type="string", dest="filename",
                           help="read midi from file" )
    (options, args) = parser.parse_args()
    try:
        midi_data = midi.read_midifile(options.filename)
    except IOError:
        print "IOError: File not found"
        
    #right hand
    idx = search_track_name(midi_data, 'right')
    keys = get_midi_keys(midi_data[idx])
    right_hand = get_formatted_notes(keys) 
    #left hand
    idx = search_track_name(midi_data, 'left')
    keys = get_midi_keys(midi_data[idx])
    left_hand = get_formatted_notes(keys) 
    #output
    plot_score(left_hand, right_hand)

if __name__=='__main__':
    main()
